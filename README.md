# Hackernews Formatter

adds some basic tweaks to hackernews:
* Increases font size for better readability
* Add even/odd row rules for article list
* Shift show/hide comment toggle to left side