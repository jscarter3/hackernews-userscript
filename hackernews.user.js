// ==UserScript==
// @name         hackernews tweaks
// @namespace    https://gitlab.com/jscarter3/hackernews-userscript
// @version      0.10
// @description  add some basic tweaks to hacker news
// @author       Jeff Carter
// @include      https://news.ycombinator.com/*
// @require      https://greasemonkey.github.io/gm4-polyfill/gm4-polyfill.js
// @require      https://unpkg.com/jss@10.0.4/dist/jss.min.js
// @require      https://unpkg.com/jss-preset-default@10.0.4/dist/jss-preset-default.min.js
// @updateURL    https://gitlab.com/jscarter3/hackernews-userscript/raw/master/hackernews.user.js
// @grant        GM_addStyle
// ==/UserScript==

/* jshint asi:true, esnext: true */

(function () {
    'use strict';

    // add elements to spacers so they can be styled
    document.querySelectorAll('.itemlist .spacer').forEach(el => {
        el.appendChild(document.createElement('td'));
        el.appendChild(document.createElement('td'));
        el.appendChild(document.createElement('td'));
    });

    addStyle();
})();

function addStyle() {
    let j = jss.create();
    j.setup(jssPresetDefault.default());
    let sheet = j.createStyleSheet({
        "@global": {
            ".title": {fontSize: "12pt"},
            ".subtext": {fontSize: "9pt"},
            ".comment-tree table": {width: "100%"},
            ".default": {
                width: "100%",
                paddingLeft: "10px",
                borderLeft: "1px solid #444",
            },
            ".togg": {
                float: "left",
                paddingRight: "5px"
            },
            "#hnmain": {background: "#fff"},
            ".itemlist": {
                width: "100%",
                "& > tbody > tr": {
                    background: "#fff",
                    "&:nth-child(6n+1)": {background: "#eee"},
                    "&:nth-child(6n+2)": {background: "#eee"},
                    "&:nth-child(6n+3)": {background: "#eee"},
                    "&:nth-child(3n+1) > td": {paddingTop: "3px"},
                }
            }
        }
    }).toString();
    GM_addStyle(sheet);
}